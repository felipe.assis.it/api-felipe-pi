const get = require('./posts_get.js')
const post = require('./posts_post.js')
const remove = require('./posts_delete.js')

module.exports = {
  get,
  post,
  remove
}
