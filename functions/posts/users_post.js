const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.titulo) return util.bind(new Error('Enter your titulo!'))
    if (!body.imagem) return util.bind(new Error('Enter your imagem!'))
    if (!body.data) return util.bind(new Error('Enter your data!'))
    if (!body.usuario_id) return util.bind(new Error('Enter your usuario_id!'))

    const insert = await mysql.query('insert into posts (titulo, imagem,data,usuario_id) values (?,?,?,?)', [body.titulo, body.imagem,data,usuario_id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
