const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const comentarios = await mysql.query('select idComentario, comentario, data, usuario_id, post_id from users where idComentario=?', [event.pathParameters.idComentario])
      return util.bind(comentarios.length ? comentarios[0] : {})
    }

    const comentarios = await mysql.query('select idComentario, comentario, data, usuario_id, post_id from Comentario')
    return util.bind(comentarios)
  } catch (error) {
    return util.bind(error)
  }
}
