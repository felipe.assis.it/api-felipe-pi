const get = require('./logs_get.js')
const post = require('./logs_post.js')

module.exports = {
  get,
  post
}
