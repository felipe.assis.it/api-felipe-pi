const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.idLogs) {
      const logs = await mysql.query('select idLogs, log, data, usuario_id from logs where idLogs=?', [event.pathParameters.idLogs])
      return util.bind(logs.length ? logs[0] : {})
    }

    const logs = await mysql.query('select idLogs, log, data, usuario_id from logs')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}
